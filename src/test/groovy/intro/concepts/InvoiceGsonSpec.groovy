package intro.concepts

import grails.plugin.json.view.test.JsonViewTest
import grails.testing.gorm.DataTest
import spock.lang.Specification

class InvoiceGsonSpec extends Specification implements JsonViewTest, DataTest {

    def setup() {
        mockDomain(Invoice)
        mockDomain(Item)
    }

    void "invoice without items"() {
        given:
        def invoice = new Invoice(name:"foobar")

        when:
        def result = render(template: "/invoice/invoice", model:[invoice:invoice])

        then:
        with(result.json) {
            name == 'foobar'
            total == 0
        }
    }

    // tag::InvoiceGsonSpecTemplate[]
    void "invoice with items"() {
        given:"an invoice with two items"
        def invoice = new Invoice(name:"foobar")
        invoice.addToItems(new Item(amount: 1_00))
        invoice.addToItems(new Item(amount: 3_00))

        when:"render json template"
        def result = render(template: "/invoice/invoice",
                model:[invoice:invoice]) // <1>

        then:"json is correct"
        with(result.json) {
            name == 'foobar'
            total == 400
            items.size() == 2
        }
    }
    // end::InvoiceGsonSpecTemplate[]

    void "item blank amount"() {
        given:
        def item = new Item(name:"foobar", amount: null)

        when:
        def result = render(template: "/invoice/item", model:[item:item])

        then:
        with(result.json) {
            name == 'foobar'
            amount == 0
        }
    }

    void "item blank name"() {
        given:
        def item = new Item(name:null, amount: 1_00)

        when:
        def result = render(template: "/invoice/item", model:[item:item])

        then:
        with(result.json) {
            name == ''
            amount == 100
        }
    }

    void "index"() {
        given:
        def invoice = new Invoice(name:"foobar")

        when:
        def result = render(view: "/invoice/index.gson", model:[invoice:invoice])

        then:
        with(result.json) {
            name == 'foobar'
            total == 0
        }
    }

    // tag::InvoiceGsonSpecView[]
    void "list view"() {
        given:"A list of invoices"
        List<Invoice> invoices = [
                new Invoice(name:"first").save(),
                new Invoice(name:"second").save()
        ]

        when:"list view is rendered"
        def result = render(view: "/invoice/list.gson", model:[invoices:invoices]) // <1>

        then:"json contains two objects"
        result.json.size() == 2
    }
    // end::InvoiceGsonSpecView[]
}
