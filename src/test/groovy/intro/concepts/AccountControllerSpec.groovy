package intro.concepts

import grails.testing.web.controllers.ControllerUnitTest
import spock.lang.Specification

class AccountControllerSpec extends Specification implements ControllerUnitTest<AccountController> {
    // Logic is covered in AccountServiceSpec
}
