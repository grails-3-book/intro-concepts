package intro.concepts

class BookTagLib {
    static defaultEncodeAs = [taglib:'none']

    static namespace = "book"

    // tag::tagLibsPrice[]
    /**
     * @attr price REQUIRED price of the book in cents // <1>
     */
    def price = { attrs ->  // <2>
        out << "<div class='price' data-price='${attrs.price}'>" // <3>
        out << g.formatNumber(  // <4>
                number: attrs.price?.div(100),
                type: "currency",
                currencyCode: "USD")
        out << "</div>"
    }
    // end::tagLibsPrice[]

    // tag::tagLibsDisplay[]
    /**
     * @attr book REQUIRED Book
     */
    def display = { attrs ->
        out << render(template: "/common/bookTagLib", model: [book: attrs.book])
    }
    // end::tagLibsDisplay[]
}
