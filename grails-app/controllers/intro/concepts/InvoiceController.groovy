package intro.concepts

import grails.transaction.Transactional


@Transactional(readOnly=true)
class InvoiceController {

    // tag::InvoiceControllerIndex[]
    def index() {
        Invoice invoice = Invoice.findById(params.long('id'),
                [fetch: [items:"join"]]) // <1>

        respond(invoice: invoice)      // <2>
    }
    // end::InvoiceControllerIndex[]

    def list() {
        List<Invoice> invoices = Invoice.findAll([fetch: [items:"join"]])

        respond(invoices: invoices)
    }
}
