package intro.concepts

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification

//@Mock([Invoice,Item])
class InvoiceSpec extends Specification implements DomainUnitTest<Invoice> {
    def testInvoice // Just a holder to make examples look better.

    def setup() {
      // tag::InvoiceTest[]
      def invoice = new Invoice(name: "Invoice 1").save()

      def widget = new Item(name: "Widget", amount: 1000)
      def bar = new Item(name: "Bar", amount: 500)

      invoice.addToItems(widget) // <1>
      invoice.addToItems(bar)
      // end::InvoiceTest[]

      testInvoice = invoice
    }

    def cleanup() { }

    void "Totals"() {
        given:
        def invoice = testInvoice

        expect:"To return total"
        invoice.total == 1500
    }
}
