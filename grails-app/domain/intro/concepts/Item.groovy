package intro.concepts

// tag::ItemClass[]
class Item {
    String name
    Integer amount // <1>

    static constraints = { }
}
// end::ItemClass[]
