package intro.concepts

class ExampleController {
  // tag::ExampleControllerIndex[]
  def index() {                               // <1>
    def myMap = [name: "Oliver", id: 123]     // <2>
    respond myMap                             // <3>
  }
  // end::ExampleControllerIndex[]

  // tag::ExampleControllerHello[]
  def hello() {
    def myMap = [greeting: "Hello ${params.name ?: 'world'}"]
    respond myMap
  }
  // end::ExampleControllerHello[]

  // tag::ExampleControllerSave[]
  def save(Person person) { // <1>
    if(!person.save()) {    // <2>
      respond person.errors // <3>
      return                // <4>
    }
    respond person          // <5>
  }
  // end::ExampleControllerSave[]

  // tag::tagLibs[]
  def tagLibs() {
    def bookList = [ books: [ // <1>
                      [id: 1, title: "Groovy In Action", price: 29_99],
                      [id: 2, title: "Java 8 In Action", price: 24_99]
                   ]]
    respond bookList
  }
  // end::tagLibs[]
}
