package intro.concepts

import grails.plugins.rest.client.RestBuilder
import spock.lang.Specification

// tag::RestSpec[]
abstract class RestSpec extends Specification {
    String baseUrl
    RestBuilder rest

    def setup() {
        baseUrl = "http://localhost:${serverPort}" // <1>
        rest = new RestBuilder()
    }
}
// end::RestSpec[]