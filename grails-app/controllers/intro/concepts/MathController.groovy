package intro.concepts

class MathController {
    def mathService // <1>

    def add() {     // <2>
        def result = [
                answer: mathService.add(params.lhs, params.rhs) // <3>
        ]

        respond result
    }

    def addCmd(AddCommand addCommand) {     // <1>
        def result

        if(addCommand.validate()) { // <2>
            result = [
                    answer: mathService.add(addCommand.lhs, addCommand.rhs)
            ]
        } else {
            result = [
                    error: addCommand.errors // <3>
            ]
        }

        respond result
    }
}

class AddCommand { // <4>
    Integer lhs
    Integer rhs // <5>
}
