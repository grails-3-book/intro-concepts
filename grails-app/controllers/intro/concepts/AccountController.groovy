package intro.concepts

import grails.transaction.Transactional

// tag::AccountController[]
class AccountController {
    def accountService // <1>

    @Transactional(readOnly = true) // <2>
    def index() {
        render Account.list() // <3>
    }

    def show() { // <4>
        render accountService.get(params.long('id'))
    }

    def transfer() {
        def result = accountService.transfer(params.long('accountIdFrom'), params.long('accountIdTo'), params.int('amount'))

        render result
    }
}
// end::AccountController[]
