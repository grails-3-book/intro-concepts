package intro.concepts

import grails.util.Environment


class BootStrap {
    def init = { servletContext ->
        // Don't pollute our Integration tests with data.
        if(Environment.current != Environment.TEST) {
            // tag::createPerson[]
            def person = new Person(firstName: "Eric",
                    lastName: "Helgeson") // <1>
            person.firstName = "Bob"                       // <2>
            assert !person.active                         // <3>
            person.save(flush: true)                        // <4>
            Person.findByFirstName("Bob")                  // <5>
            // end::createPerson[]

            new Account(name: "1", balance: 10_000_00).save(flush: true)
            new Account(name: "2", balance: 10_000_00).save(flush: true)

            new Invoice(name:"Joe's Book Order").save(flush:true)
            def invoice2 = new Invoice(name:"Eric's Book Order").save(flush:true, failOnError:true)
            invoice2.addToItems(new Item(name: "Practical Grails 3", amount: 30_00).save(flush:true))
        }
    }


    def destroy = {
    }
}
