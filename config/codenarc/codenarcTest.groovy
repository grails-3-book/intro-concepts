ruleset {
  // http://codenarc.sourceforge.net/codenarc-rule-index.html //<1>
  // ...
  DeadCode // <2>
  DoubleNegative
  EmptyTryBlock
  PrintStackTrace
  Println
  SystemErrPrint
  SystemOutPrint
  SystemExit
  ClassName { // <3>
       regex = '^[A-Z][\\$a-zA-Z0-9]*$'
  }
}
