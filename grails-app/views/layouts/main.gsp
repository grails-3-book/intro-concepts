<!DOCTYPE html> // <1>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">

    <title><g:layoutTitle default="Default Site Title"/></title> // <2>
    <asset:stylesheet href="admin.css"/> // <3>
    <g:render template="/common/googleAnalytics"/> // <4>
    <g:layoutHead/> // <5>
</head>
<body>
<g:layoutBody/> // <6>

<asset:javascript src="admin.js"/> // <7>
<asset:deferredScripts/> // <8>
</body>
</html>