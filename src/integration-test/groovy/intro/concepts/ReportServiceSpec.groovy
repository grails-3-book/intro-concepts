package intro.concepts

import grails.gorm.transactions.Rollback
import grails.testing.mixin.integration.Integration
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.Specification

@Integration
@Rollback
class ReportServiceSpec extends Specification {

    @Autowired
    ReportService reportService

    void createAccount() {
        new Account(name: "123", balance: 1_000_000_01).save(flush:true)
    }

    void "dailyBalanceReport"() {
        setup:
        createAccount()

        when:
        reportService.dailyBalanceReport()

        then:
        Account.first().balance == 1_000_000_02
    }
}
