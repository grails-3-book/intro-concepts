package intro.concepts

import grails.testing.web.controllers.ControllerUnitTest
import spock.lang.Specification

class MathControllerSpec extends Specification implements ControllerUnitTest<MathController> {

    void setup() {
        controller.mathService = new MathService()
    }

    void "adds two numbers"() {
        given:
        params.lhs = 2
        params.rhs = 4

        and:
        request.addHeader('Accept', 'application/json')

        when:
        controller.add()

        then:
        response.json.answer == 6
    }

    void "adds two strings"() {
        given:
        params.lhs = "2"
        params.rhs = "4"

        and:
        request.addHeader('Accept', 'application/json')

        when:
        controller.add()

        then:
        response.json.answer == "24"
    }

    void "command adds two numbers"() {
        given:
        params.lhs = 2
        params.rhs = 4

        and:
        request.addHeader('Accept', 'application/json')

        when:
        controller.addCmd()

        then:
        response.json.answer == 6
    }

    void "command can't add two Strings"() {
        given:
        params.lhs = "foo"
        params.rhs = "bar"

        and:
        request.addHeader('Accept', 'application/json')

        when:
        controller.addCmd()

        then:
        response.json.error
    }
}
