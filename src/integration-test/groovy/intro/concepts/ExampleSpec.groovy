package intro.concepts

import grails.testing.mixin.integration.Integration
import grails.transaction.*

import geb.spock.*
import spock.lang.Ignore

/**
 * See http://www.gebish.org/manual/current/ for more instructions
 */
@Ignore("Geb is failing on CI - ignoring for now")
// tag::ExampleSpecIntSpec[]
@Integration
@Rollback // <1>
class ExampleSpec extends GebSpec { // <2>
    void "Test Example index page"() {
        when:"The example page is loaded"
        go '/example/index' // <3>

        then:"The body is correct"
        $("body").text() == "Model: Oliver 123" // <4>
    }
}
// end::ExampleSpecIntSpec[]
