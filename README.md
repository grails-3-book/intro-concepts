# Intro Concepts

This Grails app contains all the code for the first 1/2 of the https://www.grails3book.com book.

All code examples are tested and pulled into the book from this Grails app.

# Download

You can use the download links above, or download a zip here: https://gitlab.com/grails-3-book/intro-concepts/repository/archive.zip
